/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Navigator,
    TouchableHighlight,
    Router
} from 'react-native';

// basic use of state
import Blink from './_blink';
// example of styling - with some `me` code for random assignment of styles ;)
import StyleExampleBlock from './_styleExampleBlock';

// import scenes
import FlexingScene from './scene.flexing';
import Hello from './_hello';

export default class HelloWorld extends Component {
    render() {
        const routes = [
            { title: 'Welcome to ShowRoom!', component: HelloWorldScene, index: 0 },
            { title: 'Flex Dimension Basics', component: FlexingScene, index: 1 }
        ];
        return (
            <Navigator
                initialRoute={routes[0]}
                initialRouteStack={routes}

                renderScene={(route, navigator) => {
                    switch (route.index) {
                        case 1:
                            return <FlexingScene
                                // no onForward

                                // function to call to go back to the previous scene 
                                onBack={() => {
                                    if (route.index > 0) {
                                        naviator.pop();
                                    }
                                } }

                                />;

                        case 0:
                        default:
                            return <Hello

                                onForward={() => {
                                    const nextIndex = route.index + 1;
                                    navigator.push(routes[nextIndex])
                                } }

                                // no onBack
                                />;
                    }
                } }

                navigationBar={
                    <Navigator.NavigationBar
                        routeMapper={{
                            LeftButton: (route, navigator, index, navState) => {
                                if (route.index === 0) {
                                    return null;
                                } else
                                    return (
                                        <TouchableHighlight onPress={() => navigator.pop()}>
                                            <Text>Back</Text>
                                        </TouchableHighlight>
                                    );
                            },
                            RightButton: (route, navigator, index, navState) => {
                                // not done, for now
                                if (route.index === 0) {
                                    return (
                                        <TouchableHighlight onPress={() => navigator.push(routes[1])}>
                                            <Text>Flex</Text>
                                        </TouchableHighlight>
                                    );
                                } else
                                    return null;
                            },
                            Title: (route, navigator, index, navState) =>
                            { return (<Text>Awesome Nav Bar</Text>); },
                        }}
                        style={styles.navBar}
                        />
                }

                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight
                }
                />
        );
    }
}

class HelloWorldScene extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        navigator: PropTypes.object.isRequired
    }

    static propTypes = {
        title: PropTypes.string.isRequired,
        navigator: PropTypes.object.isRequired
    }

    constructor(props, context) {
        super(props, context);
        // do I want this?
        this._onForward = this._onForward.bind(this);
        //this._onBack = this._onBack.bind(this);
    }

    _onForward() {
        this.props.naigator.push({
            title: 'Flex Dimensions Example',
            index: 1
        });
    }

    /* // Just an example - First scene, so no onBack
    _onBack() {
      this.props.navigator.pop();
    }
    */

    render() {
        return (
            <Hello />
        );
    }

}

const styles = require('./styles');

AppRegistry.registerComponent('HelloWorld', () => HelloWorld);